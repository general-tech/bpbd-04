#include <opencv2/opencv.hpp>
#include <chrono>
#include <ctime>
#include "db.h"

int main(int argc, char* argv[]) {

  if (argc < 2) {
    std::cout << "./a.out video mask.jpg" << std::endl;
    return -1;
  }


  cv::Mat img_input, img_gray, img_cropped, img_edge;
  cv::Mat mask = cv::imread(argv[2]);
  cv::cvtColor(mask, mask, cv::COLOR_RGB2GRAY);
  cv::Vec3b color_red(0,0,255);

  // load config
  int x,y,w,h;
  CvFileStorage* fs = cvOpenFileStorage("config.xml", 0, CV_STORAGE_READ);
  x = cvReadIntByName(fs, 0, "x", 0);
  y = cvReadIntByName(fs, 0, "y", 0);
  w = cvReadIntByName(fs, 0, "w", 100);
  h = cvReadIntByName(fs, 0, "h", 100);

  // load database
  DB db;
  db.init("127.0.0.1", "root", "bersaudara", "bpbd_jkt", "report", "total", argv[1]);
  auto last_time_insert = std::chrono::system_clock::now();


  // open video
  cv::VideoCapture cap(argv[1]);

  int frameCount = 0;
  float percentageWhite = 0.0;

  while(cv::waitKey(1) != 27) {

    if (!cap.isOpened()) {
      cap.open(argv[1]);
    }
    
    cap >> img_input;
    //img_cropped = img_input(cv::Rect(x,y,w,h));
    img_input(cv::Rect(x,y,w,h)).copyTo(img_cropped);

    cv::cvtColor(img_cropped, img_gray, cv::COLOR_RGB2GRAY);

    cv::bitwise_and(img_gray, mask, img_gray);
    cv::Canny(img_gray, img_edge, 200, 300, 3);
    //cv::bitwise_and(img_edge, mask, img_edge);

    for (int y=0; y<img_cropped.rows; y++) {
      for (int x=0; x<img_cropped.cols; x++) {
        cv::Point p = cv::Point(x,y);
        uchar color_edge = img_edge.at<uchar>(p);
        if (color_edge > 100) {
          img_cropped.at<cv::Vec3b>(p) = color_red;
        }
      }
    }

    auto now = std::chrono::system_clock::now();
    int duration = std::chrono::duration_cast<std::chrono::seconds>(now - last_time_insert).count();
    if (duration > 5) {
      char filename[128];
      sprintf(filename, "img/sampah-%d.jpg", frameCount);
      cv::imwrite(filename, img_cropped);
      db.insert((float) cv::countNonZero(img_edge) / (float) cv::countNonZero(mask), filename);
      last_time_insert = now;
    }

    //frameCount++;

    cv::imshow("w", img_cropped);
    // std::cout.write((char *) img_cropped.data, img_cropped.total() * img_cropped.elemSize());
    
  }
}
